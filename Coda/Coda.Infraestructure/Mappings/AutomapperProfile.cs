﻿using AutoMapper;
using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
 

namespace Coda.Infraestructure.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Mapeo de la KPIAdministrativa

            CreateMap<FlujosEfectivo, FlujosEfectivoDTOs>();
            CreateMap<FlujosEfectivoDTOs, FlujosEfectivo>();

            CreateMap<CuentasPorVencer, CuentasPorVencerDTOs>();
            CreateMap<CuentasPorVencerDTOs, CuentasPorVencer>();

            CreateMap<CuentasPorVencer, GetCuentasPorVencerPorcentajesDTOs>();
            CreateMap<GetCuentasPorVencerPorcentajesDTOs, CuentasPorVencer>();

            CreateMap<EstadodeResultados, EstadoResultadosDTOs>();
            CreateMap<EstadoResultadosDTOs, EstadodeResultados>();  

            CreateMap<ResultadosOperaciones, ResultadosOperacionDTOs>();
            CreateMap<ResultadosOperacionDTOs, ResultadosOperaciones>();

            CreateMap<Bancos, BancosDTOs>();
            CreateMap<BancosDTOs, Bancos>();

            CreateMap<VentasGeneralesTerminales, VentasGeneralesTerminalesDTOs>();
            CreateMap<VentasGeneralesTerminalesDTOs, VentasGeneralesTerminales>();

            CreateMap<ProyeccionesTerminales, ProyeccionesTerminalesDTOs>();
            CreateMap<ProyeccionesTerminalesDTOs, ProyeccionesTerminales>();

            CreateMap<CuentasPorCobrar, CuentasPorCobrarDTOs>();
            CreateMap<CuentasPorCobrarDTOs, CuentasPorCobrar>();
        }
    }
}
