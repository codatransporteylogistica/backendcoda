﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System.Collections;
using System.Data;

namespace Coda.Infraestructure.Data
{
    public class ProyeccionesTerminalesData
    {
        private readonly DbContextData _dbContextData;

        public ProyeccionesTerminalesData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }

        public async Task<List<ProyeccionesTerminales>> GetProyeccionesTerminales()
        {
            var proyeccionesTerminales = new List<ProyeccionesTerminales>();


            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "GetProyecciones", new ArrayList());

            if (ds.Tables.Count <= 0) return proyeccionesTerminales;
            proyeccionesTerminales.AddRange(from DataRow row in ds.Tables[0].Rows
                                            select new ProyeccionesTerminales
                                            {
                                                TERMINAL = row.ToStringNullable("TERMINAL"),
                                                CLIENTE = row.ToStringNullable("CLIENTE"),
                                                FletesSemana1 = row.ToStringNullable("FletesSemana1"),
                                                ViajesSemana1 = (int)row.ToIntNullable("ViajesSemana1"),
                                                FletesSemana2 = row.ToStringNullable("FletesSemana2"),
                                                ViajesSemana2 = (int)row.ToIntNullable("ViajesSemana2"),
                                                FletesSemana3 = row.ToStringNullable("FletesSemana3"),
                                                ViajesSemana3 = (int)row.ToIntNullable("ViajesSemana3"),
                                                FletesSemana4 = row.ToStringNullable("FletesSemana4"),
                                                ViajesSemana4 = (int)row.ToIntNullable("ViajesSemana4"),
                                                FletesSemana5 = row.ToStringNullable("FletesSemana5"),
                                                ViajesSemana5 = (int)row.ToIntNullable("ViajesSemana5"),
                                            });

            return proyeccionesTerminales;
        }

    }
}