﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Data
{
    public class CuentasPorCobrarData
    {
        private readonly DbContextData _dbContextData;

        public CuentasPorCobrarData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }

        public async Task<List<CuentasPorCobrar>> GetCPCProgramados()
        {
            var CuentasPorCobrarProgram = new List<CuentasPorCobrar>();

            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "GetCPCProgramados", new ArrayList());

            if (ds.Tables.Count <= 0) return CuentasPorCobrarProgram;

            CuentasPorCobrarProgram.AddRange(from DataRow row in ds.Tables[0].Rows
                                             select new CuentasPorCobrar
                                             {
                                                 CLIENTE = row.ToStringNullable("CLIENTE"),
                                                 Total = (decimal)row.ToDecimalNullable("Total"),
                                                 Programado = (decimal)row.ToDecimalNullable("Programado"),
                                                 PorProgramar = (decimal)row.ToDecimalNullable("PorProgramar")
                                             });

            return CuentasPorCobrarProgram;
        }
    }
}
