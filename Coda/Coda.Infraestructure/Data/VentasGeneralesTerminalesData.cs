﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System.Collections;
using System.Data;

namespace Coda.Infraestructure.Data
{
    public class VentasGeneralesTerminalesData
    {
        private readonly DbContextData _dbContextData;

        public VentasGeneralesTerminalesData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }

        public async Task<List<VentasGeneralesTerminales>> GetVentasGeneralesTerminales()
        {
            var ventasGeneralesTerminales = new List<VentasGeneralesTerminales>();


            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "GetVentaGeneralesTerminales", new ArrayList());

            if (ds.Tables.Count <= 0) return ventasGeneralesTerminales;
            ventasGeneralesTerminales.AddRange(from DataRow row in ds.Tables[0].Rows
                                               select new VentasGeneralesTerminales
                                               {
                                                   TERMINAL = row.ToStringNullable("TERMINAL"),
                                                   CLIENTE = row.ToStringNullable("CLIENTE"),
                                                   Semana1 = row.ToStringNullable("Semana1"),
                                                   ViajesSemana1 = (int)row.ToIntNullable("ViajesSemana1"),
                                                   Semana2 = row.ToStringNullable("Semana2"),
                                                   ViajesSemana2 = (int)row.ToIntNullable("ViajesSemana2"),
                                                   Semana3 = row.ToStringNullable("Semana3"),
                                                   ViajesSemana3 = (int)row.ToIntNullable("ViajesSemana3"),
                                                   Semana4 = row.ToStringNullable("Semana4"),
                                                   ViajesSemana4 = (int)row.ToIntNullable("ViajesSemana4"),
                                                   Semana5 = row.ToStringNullable("Semana5"),
                                                   ViajesSemana5 = (int)row.ToIntNullable("ViajesSemana5"),
                                               });

            return ventasGeneralesTerminales;
        }

    }
}
