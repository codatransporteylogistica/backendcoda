﻿using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Data
{
    public class CuentasPorVencerData
    {
        private readonly DbContextData _dbContextData;

        public CuentasPorVencerData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }

        public async Task<List<CuentasPorVencer>> GetCuentasPorVencer()
        {
            var CuentasPorVencer = new List<CuentasPorVencer>();
            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.ADVANPRO_CODA, "GETCuentasPorVencer", new ArrayList());

            if (ds.Tables.Count <= 0) return CuentasPorVencer;

            CuentasPorVencer.AddRange(from DataRow row in ds.Tables[0].Rows
                                      select new CuentasPorVencer
                                      {
                                          ClasificacionBeneficiario = row.ToStringNullable("ClasificacionBeneficiario"),
                                          SaldoTotal = (decimal)row.ToDecimalNullable("SaldoTotal"),
                                          SaldoVencido = (decimal)row.ToDecimalNullable("SaldoVencido"),
                                          PorcentajeSaldoVencido = (decimal)row.ToDecimalNullable("PorcentajeSaldoVencido"),
                                          SaldoPorVencer = (decimal)row.ToDecimalNullable("SaldoPorVencer"),
                                          PorcentajeSaldoPorVencer = (decimal)row.ToDecimalNullable("PorcentajeSaldoPorVencer")
                                      });

            return CuentasPorVencer;
        }

        public async Task<List<CuentasPorVencer>> GetCuentasPorVencerPorcentajes()
        {
            var cuentasPorVencerproncentaje = new List<CuentasPorVencer>();
            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.ADVANPRO_CODA, "GETCuentasPorVencerPorcentajes", new ArrayList());

            if (ds.Tables.Count <= 0) return cuentasPorVencerproncentaje;

            cuentasPorVencerproncentaje.AddRange(from DataRow row in ds.Tables[0].Rows
                                      select new CuentasPorVencer
                                      {
                                          ClasificacionBeneficiario = row.ToStringNullable("ClasificacionBeneficiario"),
                                          SaldoTotal = (decimal)row.ToDecimalNullable("SaldoTotal"),
                                          PorcentajeTotal =  row.ToIntNullable("PorcentajeTotal"),
                                          SaldoVencido = (decimal)row.ToDecimalNullable("SaldoVencido"),
                                          PorcentajeSaldoVencido = (decimal)row.ToDecimalNullable("PorcentajeSaldoVencido"),
                                          SaldoPorVencer = (decimal)row.ToDecimalNullable("SaldoPorVencer"),
                                          PorcentajeSaldoPorVencer = (decimal)row.ToDecimalNullable("PorcentajeSaldoPorVencer")

                                      });

            return cuentasPorVencerproncentaje;

        }
    }
}

