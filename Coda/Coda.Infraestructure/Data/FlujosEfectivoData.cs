﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Coda.Infraestructure.Data
{
    public class FlujosEfectivoData 
    {
        private readonly DbContextData _dbContextData;

        public FlujosEfectivoData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);  
        }

        public async Task<List<FlujosEfectivo>> GetFlujosEfectivos()
        {
            var flujosefectivo = new List<FlujosEfectivo>();


            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "SPGETFLUJOSEFECTIVOS", new ArrayList());

            if (ds.Tables.Count <= 0) return flujosefectivo;
            flujosefectivo.AddRange(from DataRow row in ds.Tables[0].Rows
                               select new FlujosEfectivo
                               {
                                   ID = row.ToIntNullable("ID"),
                                   CLIENTE = row.ToStringNullable("CLIENTE"),
                                   SEMANA1 = row.ToStringNullable("SEMANA1"),
                                   SEMANA2 = row.ToStringNullable("SEMANA2"),
                                   SEMANA3 = row.ToStringNullable("SEMANA3"),
                                   SEMANA4 = row.ToStringNullable("SEMANA4"),
                                   SEMANA5 = row.ToStringNullable("SEMANA5"),
                                   SEMANA6 = row.ToStringNullable("SEMANA6"),
                               });

            return flujosefectivo;
        }

    }
}
