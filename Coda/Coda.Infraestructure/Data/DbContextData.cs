﻿using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Coda.Infraestructure.Helper;

namespace Coda.Infraestructure.Data
{

    public class DbContextData
    {
        private readonly IConfiguration _configuration;
        private readonly SqlConnection sqlConnection;
        private SqlTransaction transaction;

        public DbContextData(IConfiguration config)
        {
            _configuration = config;
            sqlConnection = new SqlConnection();
        }

        private string GetConnectionStrings(string bd)
        {
            switch (bd)
            {
                case Constantes.DataBase.DWH_CODA:
                    return _configuration.GetConnectionString("cnnString");
                case Constantes.DataBase.ADVANPRO_CODA:
                    return _configuration.GetConnectionString("cnnString2");
                default:
                    return string.Empty;
            }
        }


        private void OpenConnection(string database)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.ConnectionString = GetConnectionStrings(database);
                sqlConnection.Open();
            }
        }

        private void CloseConnection()
        {
            if (sqlConnection.State == ConnectionState.Open)
            {
                sqlConnection.Close();
            }
        }

        public void ExecuteNonQuery(string database, string procedure, ArrayList parameters)
        {
            try
            {
                OpenConnection(database);

                using SqlCommand mComando = new SqlCommand(procedure, sqlConnection) { CommandType = CommandType.StoredProcedure };
                foreach (SqlParameter param in parameters)
                {
                    mComando.Parameters.Add(param);
                }

                mComando.ExecuteNonQuery();
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataSet Fill(string database, string procedure, ArrayList parameters)
        {
            DataSet mDataSet = new DataSet();

            try
            {
                OpenConnection(database);

                using (SqlCommand mComando = new SqlCommand(procedure, sqlConnection))
                {
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter param in parameters)
                    {
                        mComando.Parameters.Add(param);
                    }

                    mDataAdapter.Fill(mDataSet);
                }

                return mDataSet;
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public SqlDataReader ExecuteReader(string database, string procedure, ArrayList parameters)
        {
            SqlDataReader dataReader;
            try
            {
                OpenConnection(database);

                using (SqlCommand mComando = new SqlCommand(procedure, sqlConnection) { CommandType = CommandType.StoredProcedure })
                {
                    foreach (SqlParameter param in parameters)
                    {
                        mComando.Parameters.Add(param);
                    }

                    dataReader = mComando.ExecuteReader();
                }

                return dataReader;
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public int ExecuteScalar(string database, string procedure, ArrayList parameters)
        {
            try
            {
                OpenConnection(database);

                using SqlCommand mComando = new SqlCommand(procedure, sqlConnection) { CommandType = CommandType.StoredProcedure };
                foreach (SqlParameter param in parameters)
                {
                    mComando.Parameters.Add(param);
                }

                return Convert.ToInt32(mComando.ExecuteScalar());
                ;
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public Task<DataSet> FillAsync(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnection(database);
                    transaction = sqlConnection.BeginTransaction("Transaction.Coda.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transaction;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transaction.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transaction.Rollback();
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}