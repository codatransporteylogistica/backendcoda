﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Data
{
    public class EstadoDeResutadosData
    {
        private readonly DbContextData _dbContextData;

        public EstadoDeResutadosData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }


        public async Task<List<EstadodeResultados>> GetEstadoDeResultados()
        {
            var estadodeResltuados = new List<EstadodeResultados>();

            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.ADVANPRO_CODA, "GETObtenerResultados", new ArrayList());

            if (ds.Tables.Count <= 0) return estadodeResltuados;
            estadodeResltuados.AddRange(from DataRow row in ds.Tables[0].Rows
                                    select new EstadodeResultados
                                    {
                                        CUENTA= row.ToStringNullable("CUENTA"),
                                        GRUPO=row.ToStringNullable("GRUPO"),
                                        CARGOS= (decimal)row.ToDecimalNullable("CARGOS")
                                        //ID = row.ToIntNullable("ID"),
                                        //CLIENTE = row.ToStringNullable("CLIENTE"),
                                        //SEMANA1 = row.ToStringNullable("SEMANA1"),
                                        //SEMANA2 = row.ToStringNullable("SEMANA2"),
                                        //SEMANA3 = row.ToStringNullable("SEMANA3"),
                                        //SEMANA4 = row.ToStringNullable("SEMANA4"),
                                        //SEMANA5 = row.ToStringNullable("SEMANA5"),
                                        //SEMANA6 = row.ToStringNullable("SEMANA6"),
                                    });
            return estadodeResltuados;

        }
    }
}
