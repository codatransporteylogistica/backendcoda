﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Data
{
    public class BancosData
    {
        private readonly DbContextData _dbContextData;

        public BancosData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }

        public async Task<List<Bancos>> GetBancos()
        {
            var bancos = new List<Bancos>();


            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.ADVANPRO_CODA, "GETBANCOS", new ArrayList());

            if (ds.Tables.Count <= 0) return bancos;
            bancos.AddRange(from DataRow row in ds.Tables[0].Rows
                                    select new Bancos
                                    {
                                        Leyenda = row.ToStringNullable("leyenda"),
                                        Importe = (decimal)row.ToDecimalNullable("IMPORTE")
                                    
                                    });

            return bancos;

        }
    }
}
