﻿using Coda.Cores.Entidades;
using Coda.Infraestructure.Helper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Data
{
    public class ResultadosOperacionesData
    {
        private readonly DbContextData _dbContextData;

        public ResultadosOperacionesData(IConfiguration configuration)
        {
            _dbContextData = new DbContextData(configuration);
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesArca()
        {
            var resultadosOperaciones = new List<ResultadosOperaciones>();
            DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "PR_EDO_RESULT_RUTAS_ARCA", new ArrayList());

            if (ds.Tables.Count <= 0) return resultadosOperaciones;
            resultadosOperaciones.AddRange(from DataRow row in ds.Tables[0].Rows
                                    select new ResultadosOperaciones
                                    {
                                        Terminal = row.ToStringNullable("Terminal"),
                                        UNI_ACTIVAS= row.ToIntNullable("UNI_ACTIVAS"),
                                        CLIENTE = row.ToStringNullable("CLIENTE"),

                                        NUM_VIAJES = row.ToIntNullable("NUM_VIAJES"),
                                        TOTAL_KM = (decimal)row.ToDecimalNullable("TOTAL_KM"),
                                        INGRESO = (decimal)row.ToDecimalNullable("INGRESO"),
                                        Egresos = (decimal)row.ToDecimalNullable("Egresos"),
                                        UTILIDAD_OPE = (decimal)row.ToDecimalNullable("UTILIDAD_OPE"),
                                        UAFIRDA = (decimal)row.ToDecimalNullable("UAFIRDA"),
                                        VTA_KM = (decimal)row.ToDecimalNullable("VTA_KM"),
                                        DXL = (decimal)row.ToDecimalNullable("DXL"),
                                        DIESEL_LTS = (decimal)row.ToDecimalNullable("DIESEL_LTS"),
                                        RENDIMIENTO = (decimal)row.ToDecimalNullable("RENDIMIENTO"),
                                        DIESEIL_PAR = (decimal)row.ToDecimalNullable("DIESEIL_PAR"),
                                        VTAXUNIDAD = (decimal)row.ToDecimalNullable("VTAXUNIDAD"),
                                        VIAJESXUNIDAD = row.ToIntNullable("VIAJESXUNIDAD")

                                    });



            return resultadosOperaciones;
        }
    

    public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesGeep()
    {
        var resultadosOperaciones = new List<ResultadosOperaciones>();
        DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "PR_EDO_RESULT_RUTAS_GEEP", new ArrayList());

        if (ds.Tables.Count <= 0) return resultadosOperaciones;
        resultadosOperaciones.AddRange(from DataRow row in ds.Tables[0].Rows
                                       select new ResultadosOperaciones
                                       {
                                           Terminal = row.ToStringNullable("Terminal"),
                                           UNI_ACTIVAS = row.ToIntNullable("UNI_ACTIVAS"),
                                           CLIENTE = row.ToStringNullable("CLIENTE"),

                                           NUM_VIAJES = row.ToIntNullable("NUM_VIAJES"),
                                           TOTAL_KM = (decimal)row.ToDecimalNullable("TOTAL_KM"),
                                           INGRESO = (decimal)row.ToDecimalNullable("INGRESO"),
                                           Egresos = (decimal)row.ToDecimalNullable("Egresos"),
                                           UTILIDAD_OPE = (decimal)row.ToDecimalNullable("UTILIDAD_OPE"),
                                           UAFIRDA = (decimal)row.ToDecimalNullable("UAFIRDA"),
                                           VTA_KM = (decimal)row.ToDecimalNullable("VTA_KM"),
                                           DXL = (decimal)row.ToDecimalNullable("DXL"),
                                           DIESEL_LTS = (decimal)row.ToDecimalNullable("DIESEL_LTS"),
                                           RENDIMIENTO = (decimal)row.ToDecimalNullable("RENDIMIENTO"),
                                           DIESEIL_PAR = (decimal)row.ToDecimalNullable("DIESEIL_PAR"),
                                           VTAXUNIDAD = (decimal)row.ToDecimalNullable("VTAXUNIDAD"),
                                           VIAJESXUNIDAD = row.ToIntNullable("VIAJESXUNIDAD")


                                       });


        return resultadosOperaciones;
    }

    public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesHeineken()
    {
        var resultadosOperaciones = new List<ResultadosOperaciones>();
        DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "PR_EDO_RESULT_RUTAS_HEINEKEN", new ArrayList());

        if (ds.Tables.Count <= 0) return resultadosOperaciones;
        resultadosOperaciones.AddRange(from DataRow row in ds.Tables[0].Rows
                                       select new ResultadosOperaciones
                                       {
                                           Terminal = row.ToStringNullable("Terminal"),
                                           UNI_ACTIVAS = row.ToIntNullable("UNI_ACTIVAS"),
                                           CLIENTE = row.ToStringNullable("CLIENTE"),

                                           NUM_VIAJES = row.ToIntNullable("NUM_VIAJES"),
                                           TOTAL_KM = (decimal)row.ToDecimalNullable("TOTAL_KM"),
                                           INGRESO = (decimal)row.ToDecimalNullable("INGRESO"),
                                           Egresos = (decimal)row.ToDecimalNullable("Egresos"),
                                           UTILIDAD_OPE = (decimal)row.ToDecimalNullable("UTILIDAD_OPE"),
                                           UAFIRDA = (decimal)row.ToDecimalNullable("UAFIRDA"),
                                           VTA_KM = (decimal)row.ToDecimalNullable("VTA_KM"),
                                           DXL = (decimal)row.ToDecimalNullable("DXL"),
                                           DIESEL_LTS = (decimal)row.ToDecimalNullable("DIESEL_LTS"),
                                           RENDIMIENTO = (decimal)row.ToDecimalNullable("RENDIMIENTO"),
                                           DIESEIL_PAR = (decimal)row.ToDecimalNullable("DIESEIL_PAR"),
                                           VTAXUNIDAD = (decimal)row.ToDecimalNullable("VTAXUNIDAD"),
                                           VIAJESXUNIDAD = row.ToIntNullable("VIAJESXUNIDAD")


                                       });


        return resultadosOperaciones;
    }

    public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesModelo()
    {
        var resultadosOperaciones = new List<ResultadosOperaciones>();
        DataSet ds = await _dbContextData.FillAsync(Constantes.DataBase.DWH_CODA, "PR_EDO_RESULT_RUTAS_MODELO", new ArrayList());

        if (ds.Tables.Count <= 0) return resultadosOperaciones;
        resultadosOperaciones.AddRange(from DataRow row in ds.Tables[0].Rows
                                       select new ResultadosOperaciones
                                       {
                                           Terminal = row.ToStringNullable("Terminal"),
                                           UNI_ACTIVAS = row.ToIntNullable("UNI_ACTIVAS"),
                                           CLIENTE = row.ToStringNullable("CLIENTE"),

                                           NUM_VIAJES = row.ToIntNullable("NUM_VIAJES"),
                                           TOTAL_KM = (decimal)row.ToDecimalNullable("TOTAL_KM"),
                                           INGRESO = (decimal)row.ToDecimalNullable("INGRESO"),
                                           Egresos = (decimal)row.ToDecimalNullable("Egresos"),
                                           UTILIDAD_OPE = (decimal)row.ToDecimalNullable("UTILIDAD_OPE"),
                                           UAFIRDA = (decimal)row.ToDecimalNullable("UAFIRDA"),
                                           VTA_KM = (decimal)row.ToDecimalNullable("VTA_KM"),
                                           DXL = (decimal)row.ToDecimalNullable("DXL"),
                                           DIESEL_LTS = (decimal)row.ToDecimalNullable("DIESEL_LTS"),
                                           RENDIMIENTO = (decimal)row.ToDecimalNullable("RENDIMIENTO"),
                                           DIESEIL_PAR = (decimal)row.ToDecimalNullable("DIESEIL_PAR"),
                                           VTAXUNIDAD = (decimal)row.ToDecimalNullable("VTAXUNIDAD"),
                                           VIAJESXUNIDAD = row.ToIntNullable("VIAJESXUNIDAD")


                                       });


        return resultadosOperaciones;
    }
}
}
