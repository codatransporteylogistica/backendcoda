﻿

namespace Coda.Infraestructure.Helper
{

    public class Constantes
    {
        public static class DataBase
        {
            public const string DWH_CODA = "DWH_CODA";
            public const string ADVANPRO_CODA = "ADVANPRO_CODA";
        }

        public static class Procedures
        {
        }

        public static class Message
        {
            public const string M100 = "Success";
            public const string M101 = "Review log records";
            public const string HMACSHA512 = "Password erroneo, favor de verificar la información";
        }
        public static class Accion
        {
            public const string A101 = "Respuesta Solicitud";
            public const string A102 = "Nueva Solicitud";
            public const string A103 = "Insertar";
            public const string A104 = "Actualizar";
            public const string A105 = "Eliminar";
            public const string A106 = "Seleccionar";

            public const string A999 = "Error";
        }
        public static class Encryptor
        {
            public const string HMACSHA512 = "HMACSHA512";
        }
    }
}