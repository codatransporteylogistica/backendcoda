﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Helper
{
    public static class ConvertDbValue
    {
        public static int? ToIntNullable(this DataRow row, string columnName)
        {
            return CastAsIntNullable(row[columnName]);
        }

        private static int? CastAsIntNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            int? value = (hasValue ? (int?)o : (int?)null);
            return value;
        }

        public static string? ToStringNullable(this DataRow row, string columnName)
        {
            return CastAsStringNullable(row[columnName]);
        }

        private static string? CastAsStringNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            string? value = (hasValue ? o.ToString() : null);
            return value;
        }

        public static DateTime? ToDateNullable(this DataRow row, string columnName)
        {
            return CastAsDateNullable(row[columnName]);
        }

        private static DateTime? CastAsDateNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            DateTime? value = (hasValue ? (DateTime?)o : (DateTime?)null);
            return value;
        }

        public static decimal? ToDecimalNullable(this DataRow row, string columnName)
        {
            return CastAsDecimalNullable(row[columnName]);
        }

        private static decimal? CastAsDecimalNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            decimal? value = (hasValue ? (decimal?)o : (decimal?)null);
            return value;
        }

        public static Int16? ToInt16Nullable(this DataRow row, string columnName)
        {
            return CastAsInt16Nullable(row[columnName]);
        }

        private static Int16? CastAsInt16Nullable(object o)
        {
            bool hasValue = !(o is DBNull);
            Int16? value = (hasValue ? (Int16?)o : (Int16?)null);
            return value;
        }

        public static bool? ToBoolNullable(this DataRow row, string columnName)
        {
            return CastAsBoolNullable(row[columnName]);
        }

        private static bool? CastAsBoolNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            bool? value = (hasValue ? (bool?)o : (bool?)null);
            return value;
        }

        public static byte? ToByteNullable(this DataRow row, string columnName)
        {
            return CastAsByteNullable(row[columnName]);
        }

        private static byte? CastAsByteNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            byte? value = (hasValue ? (byte?)o : (byte?)null);
            return value;
        }
    }
}