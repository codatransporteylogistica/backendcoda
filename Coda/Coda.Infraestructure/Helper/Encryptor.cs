﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Helper
{
    public class Encryptor
    {
        public static string EncryptorPassword(string message, string key, string tipo)
        {
            string resultado = string.Empty;

            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

            byte[] keyByte = encoding.GetBytes(key);

            HMACMD5 hmacmd5 = new HMACMD5(keyByte);
            HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);
            HMACSHA384 hmacsha384 = new HMACSHA384(keyByte);
            HMACSHA512 hmacsha512 = new HMACSHA512(keyByte);

            byte[] messageBytes = encoding.GetBytes(message);

            byte[] hashmessage;

            switch (tipo)
            {
                case "HMACMD5":
                    hashmessage = hmacmd5.ComputeHash(messageBytes);
                    resultado = ByteToString(hashmessage);
                    break;
                case "HMACSHA1":
                    hashmessage = hmacsha1.ComputeHash(messageBytes);
                    resultado = ByteToString(hashmessage);
                    break;
                case "HMACSHA256":
                    hashmessage = hmacsha256.ComputeHash(messageBytes);
                    resultado = ByteToString(hashmessage);
                    break;
                case "HMACSHA384":
                    hashmessage = hmacsha384.ComputeHash(messageBytes);
                    resultado = ByteToString(hashmessage);
                    break;
                case "HMACSHA512":
                    hashmessage = hmacsha512.ComputeHash(messageBytes);
                    resultado = ByteToString(hashmessage);
                    break;
            }

            return resultado;
        }

        public static string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }
    }
}
