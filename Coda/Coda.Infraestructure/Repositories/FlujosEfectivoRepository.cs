﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;
 

namespace Coda.Infraestructure.Repositories
{
    public class FlujosEfectivoRepository : IFlujosEfectivoRepository
    {
        private readonly FlujosEfectivoData _flujosEfectivoData;

        public FlujosEfectivoRepository(IConfiguration configuration)
        {
            _flujosEfectivoData = new FlujosEfectivoData(configuration);
        }
        public async Task<List<FlujosEfectivo>> GetFlujosEfectivos()
        {
            return await _flujosEfectivoData.GetFlujosEfectivos();
        }
    }
}
