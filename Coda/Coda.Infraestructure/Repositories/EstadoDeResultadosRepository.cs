﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Repositories
{
    public class EstadoDeResultadosRepository : IEstadosDeResultadosRepository
    {

        private readonly EstadoDeResutadosData _estadoDeResutadosData;

        public EstadoDeResultadosRepository(IConfiguration configuration)
        {
            _estadoDeResutadosData = new EstadoDeResutadosData(configuration);
        }

        public async Task<List<EstadodeResultados>> GetEstadoDeResultados()
        {
            return await _estadoDeResutadosData.GetEstadoDeResultados();
        }
    }
}
