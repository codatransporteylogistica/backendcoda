﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Coda.Infraestructure.Repositories;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Repositories
{
    public class BancosRepository : IBancosRepository
    {
        private readonly BancosData _bancosData;

        public BancosRepository(IConfiguration configuration)
        {
            _bancosData = new BancosData(configuration);
        }
        public async Task<List<Bancos>> GetBancos()
        {
            return await _bancosData.GetBancos();
        }
    }
} 