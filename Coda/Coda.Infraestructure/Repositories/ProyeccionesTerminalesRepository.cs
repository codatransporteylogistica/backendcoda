﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;

namespace Coda.Infraestructure.Repositories
{
    public class ProyeccionesTerminalesRepository : IProyeccionesTerminalesRepository
    {
        private readonly ProyeccionesTerminalesData _proyeccionesTerminalesData;

        public ProyeccionesTerminalesRepository(IConfiguration configuration)
        {
            _proyeccionesTerminalesData = new ProyeccionesTerminalesData(configuration);
        }
        public async Task<List<ProyeccionesTerminales>> GetProyeccionesTerminales()
        {
            return await _proyeccionesTerminalesData.GetProyeccionesTerminales();
        }
    }
}
