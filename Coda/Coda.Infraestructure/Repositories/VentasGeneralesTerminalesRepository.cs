﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;

namespace Coda.Infraestructure.Repositories
{
    public class VentasGeneralesTerminalesRepository : IVentasGeneralesTerminalesRepository
    {
        private readonly VentasGeneralesTerminalesData _ventasGeneralesTerminalesData;

        public VentasGeneralesTerminalesRepository(IConfiguration configuration)
        {
            _ventasGeneralesTerminalesData = new VentasGeneralesTerminalesData(configuration);
        }
        public async Task<List<VentasGeneralesTerminales>> GetVentasGeneralesTerminales()
        {
            return await _ventasGeneralesTerminalesData.GetVentasGeneralesTerminales();
        }
    }
}
