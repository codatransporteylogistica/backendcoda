﻿using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;
 

namespace Coda.Infraestructure.Repositories
{
    public class CuentasPorVencerRepository : ICuentasPorVencerRepository
    {

        private readonly CuentasPorVencerData _cuentasPorVencerData;    

        public CuentasPorVencerRepository(IConfiguration configuration)
        {
            _cuentasPorVencerData=new CuentasPorVencerData(configuration);
        }

     public async Task<List<CuentasPorVencer>> GetCuentasPorVencer()
        {
        
            return await _cuentasPorVencerData.GetCuentasPorVencer();
        }

        public async Task<List<CuentasPorVencer>> GetCuentasPorVencerPorcentajes()
        {
            return await _cuentasPorVencerData.GetCuentasPorVencerPorcentajes();
        }
    }
}
