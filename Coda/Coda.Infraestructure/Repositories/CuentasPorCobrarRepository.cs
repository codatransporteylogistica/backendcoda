﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Infraestructure.Repositories
{
    public class CuentasPorCobrarRepository : ICuentasPorCobrarRepository
    {

        private readonly CuentasPorCobrarData _cuentasPorCobrarData;

        public CuentasPorCobrarRepository(IConfiguration configuration)
        {
            _cuentasPorCobrarData = new CuentasPorCobrarData(configuration);
        }

        public async Task<List<CuentasPorCobrar>> GetCPCProgramados()
        {
            return await _cuentasPorCobrarData.GetCPCProgramados();
        }
    }
}
