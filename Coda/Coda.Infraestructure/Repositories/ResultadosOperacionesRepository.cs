﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Infraestructure.Data;
using Microsoft.Extensions.Configuration;

namespace Coda.Infraestructure.Repositories
{
    public class ResultadosOperacionesRepository : IResultadosOperacionRepository
    {

        private readonly ResultadosOperacionesData _resultadosOperacionesData;

        public ResultadosOperacionesRepository(IConfiguration configuration)
        {
            _resultadosOperacionesData = new ResultadosOperacionesData(configuration);
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesArca()
        {
            return await _resultadosOperacionesData.GetResultadoOperacionesArca();
        }


        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesGeep()
        {
            return await _resultadosOperacionesData.GetResultadoOperacionesGeep();
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesHeineken()
        {
            return await _resultadosOperacionesData.GetResultadoOperacionesHeineken();
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesModelo()
        {
            return await _resultadosOperacionesData.GetResultadoOperacionesModelo();
        }
    }
}
