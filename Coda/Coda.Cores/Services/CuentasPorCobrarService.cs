﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;


namespace Coda.Cores.Services
{
    public class CuentasPorCobrarService : ICuentasPorCobrarServices
    {
        private readonly ICuentasPorCobrarRepository _cuentasPorCobrarRepository;

        public CuentasPorCobrarService(ICuentasPorCobrarRepository cuentasPorCobraRepository)
        {
            _cuentasPorCobrarRepository = cuentasPorCobraRepository;
        }
        public async Task<List<CuentasPorCobrar>> GetCPCProgramados()
        {
            return await _cuentasPorCobrarRepository.GetCPCProgramados();
        }

    }
}
