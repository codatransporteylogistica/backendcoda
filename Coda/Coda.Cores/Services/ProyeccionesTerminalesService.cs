﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Services
{
    public class ProyeccionesTerminalesService : IProyeccionesTerminalesServices
    {
        private readonly IProyeccionesTerminalesRepository _proyeccionesTerminalesRepository;

        public ProyeccionesTerminalesService(IProyeccionesTerminalesRepository proyeccionesTerminales)
        {
            _proyeccionesTerminalesRepository = proyeccionesTerminales;
        }
        public async Task<List<ProyeccionesTerminales>> GetProyeccionesTerminales()
        {
            return await _proyeccionesTerminalesRepository.GetProyeccionesTerminales();
        }
    }
}
