﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
 

namespace Coda.Cores.Services
{
    public class FlujosEfectivoService : IFlujosEfectivoServices
    { 
        private readonly IFlujosEfectivoRepository _flujosEfectivoRepository;

    public FlujosEfectivoService(IFlujosEfectivoRepository flujosRepository)
    {
        _flujosEfectivoRepository = flujosRepository;
    }
    
        public async Task<List<FlujosEfectivo>> GetFlujosEfectivos()
        {
            return await _flujosEfectivoRepository.GetFlujosEfectivos();
        }
    }
}
