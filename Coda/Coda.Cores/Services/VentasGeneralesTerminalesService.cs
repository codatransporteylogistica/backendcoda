﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Services
{
    public class VentasGeneralesTerminalesService : IVentasGeneralesTerminalesServices
    {
        private readonly IVentasGeneralesTerminalesRepository _ventasGeneralesTerminalesRepository;

        public VentasGeneralesTerminalesService(IVentasGeneralesTerminalesRepository ventasGeneralesRepository)
        {
            _ventasGeneralesTerminalesRepository = ventasGeneralesRepository;
        }

        public async Task<List<VentasGeneralesTerminales>> GetVentasGeneralesTerminales()
        {
            return await _ventasGeneralesTerminalesRepository.GetVentasGeneralesTerminales();
        }
    }
}
