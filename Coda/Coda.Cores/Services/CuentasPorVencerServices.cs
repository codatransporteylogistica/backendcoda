﻿using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Services
{
    public class CuentasPorVencerServices : ICuentasPorVencerServices
    {
        private readonly ICuentasPorVencerRepository _cuentasPorVencerRepository;

        public CuentasPorVencerServices(ICuentasPorVencerRepository cuentasPorVencerRepository)
        {
            _cuentasPorVencerRepository = cuentasPorVencerRepository;
        }


        public async Task<List<CuentasPorVencer>> GetCuentasPorVencer()
        {
          return await _cuentasPorVencerRepository.GetCuentasPorVencer();
        }

        public async Task<List<CuentasPorVencer>> GetCuentasPorVencerPorcentajes()
        {
            return await _cuentasPorVencerRepository.GetCuentasPorVencerPorcentajes();
        }
    }
}
