﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Services
{
    public class ResultadosOperacionesServices : IResultadosOperacionServices
    {
        private readonly IResultadosOperacionRepository _resultadosOperacionRepository;

        public ResultadosOperacionesServices(IResultadosOperacionRepository resultadosOperacionRepository)
        {
            _resultadosOperacionRepository = resultadosOperacionRepository;
        }
        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesArca()
        {
            return await _resultadosOperacionRepository.GetResultadoOperacionesArca();
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesGeep()
        {
            return await _resultadosOperacionRepository.GetResultadoOperacionesGeep();
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesHeineken()
        {
            return await _resultadosOperacionRepository.GetResultadoOperacionesHeineken();
        }

        public async Task<List<ResultadosOperaciones>> GetResultadoOperacionesModelo()
        {
            return await _resultadosOperacionRepository.GetResultadoOperacionesModelo();
        }
    }
}
