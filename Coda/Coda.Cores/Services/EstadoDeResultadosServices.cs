﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Services
{
    public class EstadoDeResultadosServices : IEstadoDeResultadosServices
    {
        private readonly IEstadosDeResultadosRepository _estadosDeResultadosRepository;

        public EstadoDeResultadosServices(IEstadosDeResultadosRepository estadosDeResultadosRepository)
        {
            _estadosDeResultadosRepository = estadosDeResultadosRepository;
        }

        public async Task<List<EstadodeResultados>> GetEstadoDeResultados()
        {
            return await _estadosDeResultadosRepository.GetEstadoDeResultados();
        }
    }
}
