﻿using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Cores.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Services
{
    public class BancosServices : IBancosServices
    {
        private readonly IBancosRepository _bancosRepository;

        public BancosServices(IBancosRepository bancosRepository)
        {
            _bancosRepository = bancosRepository;
        }

        public async Task<List<Bancos>> GetBancos()
        {
            return await _bancosRepository.GetBancos();        }
    }
} 