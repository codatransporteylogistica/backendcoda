﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.DTOs
{
    public class CuentasPorCobrarDTOs
    {
        public string CLIENTE { get; set; }
        public decimal? Total { get; set; }
        public decimal? Programado { get; set; }
        public decimal? PorProgramar { get; set; }
    }
}
