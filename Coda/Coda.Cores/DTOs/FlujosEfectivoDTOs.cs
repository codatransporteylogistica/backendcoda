﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.DTOs
{
    public class FlujosEfectivoDTOs
    {
        public int? ID { get; set; }
        public string? CLIENTE { get; set; }
        public string? SEMANA1 { get; set; }
        public string? SEMANA2 { get; set; }
        public string? SEMANA3 { get; set; }
        public string? SEMANA4 { get; set; }
        public string? SEMANA5 { get; set; }
        public string? SEMANA6 { get; set; }

    }
}
