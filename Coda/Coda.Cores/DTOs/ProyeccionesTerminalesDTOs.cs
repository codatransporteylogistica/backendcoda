﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.DTOs
{
    public class ProyeccionesTerminalesDTOs
    {
        public string TERMINAL { get; set; }
        public string CLIENTE { get; set; }
        public string FletesSemana1 { get; set; }
        public int ViajesSemana1 { get; set; }
        public string FletesSemana2 { get; set; }
        public int ViajesSemana2 { get; set; }
        public string FletesSemana3 { get; set; }
        public int ViajesSemana3 { get; set; }
        public string FletesSemana4 { get; set; }
        public int ViajesSemana4 { get; set; }
        public string FletesSemana5 { get; set; }
        public int ViajesSemana5 { get; set; }

    }
}
