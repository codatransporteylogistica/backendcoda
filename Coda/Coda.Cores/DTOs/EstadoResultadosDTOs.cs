﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.DTOs
{
    public class EstadoResultadosDTOs
    {
        public string? CUENTA { get; set; }
        public string? GRUPO { get; set; }
        public decimal? CARGOS { get; set; }
    }
}
