﻿using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Interfaces
{
   public interface ICuentasPorVencerServices
    {
        Task<List<CuentasPorVencer>> GetCuentasPorVencer();
        Task<List<CuentasPorVencer>> GetCuentasPorVencerPorcentajes();
    }
}
