﻿using Coda.Cores.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Interfaces
{
    public interface IFlujosEfectivoServices
    {
        Task<List<FlujosEfectivo>> GetFlujosEfectivos();
    }
}
