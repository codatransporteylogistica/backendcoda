﻿using Coda.Cores.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Interfaces
{
    public interface IResultadosOperacionServices
    {
        Task<List<ResultadosOperaciones>> GetResultadoOperacionesArca();
        Task<List<ResultadosOperaciones>> GetResultadoOperacionesHeineken();
        Task<List<ResultadosOperaciones>> GetResultadoOperacionesModelo();
        Task<List<ResultadosOperaciones>> GetResultadoOperacionesGeep();
    }
}
