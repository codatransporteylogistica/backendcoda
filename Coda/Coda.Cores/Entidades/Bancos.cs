﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Entidades
{
    public class Bancos
    {
        public string? Leyenda {  get; set; }
        public decimal? Importe { get; set; }
    }
}
