﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Entidades
{
    public class VentasGeneralesTerminales
    {
        public string TERMINAL { get; set; }
        public string CLIENTE { get; set; }
        public string Semana1 { get; set; }
        public int ViajesSemana1 { get; set; }
        public string Semana2 { get; set; }
        public int ViajesSemana2 { get; set; }
        public string Semana3 { get; set; }
        public int ViajesSemana3 { get; set; }
        public string Semana4 { get; set; }
        public int ViajesSemana4 { get; set; }
        public string Semana5 { get; set; }
        public int ViajesSemana5 { get; set; }

    }
}
