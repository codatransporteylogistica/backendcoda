﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Entidades
{
    public class CuentasPorVencer
    {
        public string ClasificacionBeneficiario { get; set; }
        public decimal? SaldoTotal { get; set; }
        public decimal? PorcentajeTotal { get; set; }
        public decimal? SaldoVencido { get; set; }
        public decimal? PorcentajeSaldoVencido { get; set; }
        public decimal? SaldoPorVencer { get; set; }
        public decimal? PorcentajeSaldoPorVencer { get; set; }
    }
}
