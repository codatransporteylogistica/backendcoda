﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coda.Cores.Entidades
{
    public class ResultadosOperaciones
    {
        public string? Terminal { get; set; }
        public int? UNI_ACTIVAS { get; set; }
        public string? CLIENTE { get; set; }
        public int? NUM_VIAJES { get; set; }
        public decimal? TOTAL_KM { get; set; }
        public decimal? INGRESO { get; set; }
        public decimal? Egresos { get; set; }
        public decimal? UTILIDAD_OPE { get; set; }
        public decimal? UAFIRDA { get; set; }
        public decimal? VTA_KM { get; set; }
        public decimal? DXL { get; set; }
        public decimal? DIESEL_LTS { get; set; }
        public decimal? RENDIMIENTO { get; set; }
        public decimal? DIESEIL_PAR { get; set; }
        public decimal? VTAXUNIDAD { get; set; }
        public int? VIAJESXUNIDAD { get; set; }

    }
}
