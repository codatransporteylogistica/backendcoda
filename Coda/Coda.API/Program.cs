using Coda.Cores.Interfaces;
using Coda.Cores.Services;
using Coda.Infraestructure.Repositories;
 

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());


builder.Services.AddControllers();




builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    // builder.WithOrigins("http://192.168.16.160:8081").AllowAnyMethod().AllowAnyHeader();
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();

}));


//DI
builder.Services.AddTransient<IFlujosEfectivoServices, FlujosEfectivoService>();
builder.Services.AddTransient<IFlujosEfectivoRepository, FlujosEfectivoRepository>();
builder.Services.AddTransient<ICuentasPorVencerServices, CuentasPorVencerServices>();
builder.Services.AddTransient<ICuentasPorVencerRepository, CuentasPorVencerRepository>();
builder.Services.AddTransient<IEstadoDeResultadosServices, EstadoDeResultadosServices>();
builder.Services.AddTransient<IEstadosDeResultadosRepository, EstadoDeResultadosRepository>();
builder.Services.AddTransient<IResultadosOperacionServices, ResultadosOperacionesServices>();
builder.Services.AddTransient<IResultadosOperacionRepository, ResultadosOperacionesRepository>();
builder.Services.AddTransient<IBancosServices, BancosServices>();
builder.Services.AddTransient<IBancosRepository, BancosRepository>();
builder.Services.AddTransient<IVentasGeneralesTerminalesServices, VentasGeneralesTerminalesService>();
builder.Services.AddTransient<IVentasGeneralesTerminalesRepository, VentasGeneralesTerminalesRepository>();
builder.Services.AddTransient<IProyeccionesTerminalesServices, ProyeccionesTerminalesService>();
builder.Services.AddTransient<IProyeccionesTerminalesRepository, ProyeccionesTerminalesRepository>();
builder.Services.AddTransient<ICuentasPorCobrarServices, CuentasPorCobrarService>();
builder.Services.AddTransient<ICuentasPorCobrarRepository, CuentasPorCobrarRepository>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(/*options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Coda",
        Description = "An ASP.NET Core Web API for managing Security"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
}*/);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors("corsapp");

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseStaticFiles();

app.MapControllers();

app.Run();
