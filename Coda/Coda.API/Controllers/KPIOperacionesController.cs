﻿using AutoMapper;
using Coda.API.Result;
using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
using Coda.Cores.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Coda.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KPIOperacionesController : ControllerBase
    {
        private IConfiguration Configuration { get; }
        private readonly IResultadosOperacionServices _resultadosOperacionServices;
        private readonly IVentasGeneralesTerminalesServices _ventasGeneralesTerminalesServices;
        private readonly IProyeccionesTerminalesServices _proyeccionesTerminalesServices;
        private readonly IMapper _mapper;


        public KPIOperacionesController(IMapper mapper, IConfiguration configuration, IResultadosOperacionServices resultadosOperacionServices,
            IVentasGeneralesTerminalesServices ventasGeneralesTerminalesServices, IProyeccionesTerminalesServices proyeccionesTerminalesServices)
        {
            _mapper = mapper;
            Configuration = configuration;
            _resultadosOperacionServices= resultadosOperacionServices;
            _proyeccionesTerminalesServices = proyeccionesTerminalesServices;
            _ventasGeneralesTerminalesServices = ventasGeneralesTerminalesServices;
        }

        [HttpGet]
        [Route("GetResultadoOperacionesArca")]
        public async Task<IActionResult> GetResultadoOperacionesArca()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ResultadosOperaciones> resultadosOperaciones = await _resultadosOperacionServices.GetResultadoOperacionesArca();
                    var resultadosOperacionesDTOs = _mapper.Map<List<ResultadosOperacionDTOs>>(resultadosOperaciones);
                    return new OKResult(resultadosOperacionesDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GetResultadoOperacionesGeep")]
        public async Task<IActionResult> GetResultadoOperacionesGeep()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ResultadosOperaciones> resultadosOperaciones = await _resultadosOperacionServices.GetResultadoOperacionesGeep();
                    var resultadosOperacionesDTOs = _mapper.Map<List<ResultadosOperacionDTOs>>(resultadosOperaciones);
                    return new OKResult(resultadosOperacionesDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }


        [HttpGet]
        [Route("GetResultadoOperacionesHeineken")]
        public async Task<IActionResult> GetResultadoOperacionesHeineken()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ResultadosOperaciones> resultadosOperaciones = await _resultadosOperacionServices.GetResultadoOperacionesHeineken();
                    var resultadosOperacionesDTOs = _mapper.Map<List<ResultadosOperacionDTOs>>(resultadosOperaciones);
                    return new OKResult(resultadosOperacionesDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }


        [HttpGet]
        [Route("GetResultadoOperacionesModelo")]
        public async Task<IActionResult> GetResultadoOperacionesModelo()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ResultadosOperaciones> resultadosOperaciones = await _resultadosOperacionServices.GetResultadoOperacionesModelo();
                    var resultadosOperacionesDTOs = _mapper.Map<List<ResultadosOperacionDTOs>>(resultadosOperaciones);
                    return new OKResult(resultadosOperacionesDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }


        [HttpGet]
        [Route("GetVentaGeneralesTerminales")]
        public async Task<IActionResult> GetVentasGeneralesTerminales()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<VentasGeneralesTerminales> ventasGeneralesTerminales = await _ventasGeneralesTerminalesServices.GetVentasGeneralesTerminales();
                    var ventasGeneralesTerminalesDTOs = _mapper.Map<List<VentasGeneralesTerminalesDTOs>>(ventasGeneralesTerminales);
                    return new OKResult(ventasGeneralesTerminalesDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GetProyecciones")]
        public async Task<IActionResult> GetProyeccionesTerminales()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<ProyeccionesTerminales> proyeccionesTerminales = await _proyeccionesTerminalesServices.GetProyeccionesTerminales();
                    var proyeccionesTerminalesDTOs = _mapper.Map<List<ProyeccionesTerminales>>(proyeccionesTerminales);
                    return new OKResult(proyeccionesTerminales);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }


    }
}
