﻿using AutoMapper;
using Coda.API.Result;
using Coda.Cores.DTOs;
using Coda.Cores.Entidades;
using Coda.Cores.Interfaces;
 
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Coda.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KAPIAdministrativoController : ControllerBase
    {
        private IConfiguration Configuration { get; }
        private readonly IFlujosEfectivoServices _flujosEfectivoServices;
        private readonly ICuentasPorVencerServices _cuentasPorVencerServices;
        private readonly IEstadoDeResultadosServices _estadoDeResultadosServices;
        private readonly IBancosServices _bancosServices;
        private readonly ICuentasPorCobrarServices _cuentasPorCobrar;
        private readonly IMapper _mapper;


        public KAPIAdministrativoController(IMapper mapper, IConfiguration configuration, IFlujosEfectivoServices flujosEfectivoServices,
            ICuentasPorVencerServices cuentasPorVencerServices, IEstadoDeResultadosServices estadoDeResultadosServices, IBancosServices bancosServices, ICuentasPorCobrarServices cuentasPorCobrar)
        {
            _mapper = mapper;
            Configuration = configuration;
            _flujosEfectivoServices = flujosEfectivoServices;
            _cuentasPorVencerServices = cuentasPorVencerServices;
            _estadoDeResultadosServices = estadoDeResultadosServices;
            _bancosServices = bancosServices;
            _cuentasPorCobrar = cuentasPorCobrar;
        }

        [HttpGet]
        [Route("GetCuentasPorVencer")]
        public async Task<IActionResult> GetCuentasPorVencer()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<CuentasPorVencer> cuentasPorVencer = await _cuentasPorVencerServices.GetCuentasPorVencer();
                    var cuentasPorVencerDTOs = _mapper.Map<List<CuentasPorVencerDTOs>>(cuentasPorVencer);
                    return new OKResult(cuentasPorVencerDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GetCuentasPorVencerPorcentajes")]
        public async Task<IActionResult> GetCuentasPorVencerPorcentaje()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<CuentasPorVencer> cuentasPorVencerPorcentaje = await _cuentasPorVencerServices.GetCuentasPorVencerPorcentajes();
                    var cuentasPorVencerPorcentajesDTOs = _mapper.Map <List<GetCuentasPorVencerPorcentajesDTOs>> (cuentasPorVencerPorcentaje);
                    return new OKResult(cuentasPorVencerPorcentajesDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GetFlujosEfectivo")]
        public async Task<IActionResult> GetFlujosEfectivos()
        {

            try
            {
                if (ModelState.IsValid)
                {
                    List<FlujosEfectivo> flujosEfectivos = await _flujosEfectivoServices.GetFlujosEfectivos();
                    var flujosEfectivoDTOs = _mapper.Map<List<FlujosEfectivoDTOs>>(flujosEfectivos);
                    return new OKResult(flujosEfectivoDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GETObtenerResultados")]
        public async Task<IActionResult> GetEstadoDeResultados()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<EstadodeResultados> estadodeResultados = await _estadoDeResultadosServices.GetEstadoDeResultados();
                    var estadoResultadosDTOs = _mapper.Map<List<EstadoResultadosDTOs>>(estadodeResultados);
                    return new OKResult(estadoResultadosDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GETBancos")]
        public async Task<IActionResult> GetBancos()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<Bancos> bancos = await _bancosServices.GetBancos();
                    var bancosDTOs = _mapper.Map<List<BancosDTOs>>(bancos);
                    return new OKResult(bancosDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        [HttpGet]
        [Route("GetCPCProgramados")]
        public async Task<IActionResult> GetCPCProgramados()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<CuentasPorCobrar> cuentasPorCobrarProgram = await _cuentasPorCobrar.GetCPCProgramados();
                    var cuentasPorCobrarProgramDTOs = _mapper.Map<List<CuentasPorCobrarDTOs>>(cuentasPorCobrarProgram);
                    return new OKResult(cuentasPorCobrarProgramDTOs);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string? error = message.ErrorMessage != string.Empty ? message.ErrorMessage : message.Exception?.Message;

                    // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                    //     Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                // if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString().ToLower() == "true")
                //     Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404", ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }


    }
}

