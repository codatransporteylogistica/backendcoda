﻿using Microsoft.AspNetCore.Mvc;

namespace Coda.API.Result
{
    public class OKResult : IActionResult
    {
        private readonly OkContent _content;

        public OKResult(object data)
        {
            _content = new OkContent()
            {
                data = data
            };
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var objectResult = new ObjectResult(_content.data)
            {
                StatusCode = StatusCodes.Status200OK
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }

    public class OkContent
    {
        public object data { get; set; }
    }
}
