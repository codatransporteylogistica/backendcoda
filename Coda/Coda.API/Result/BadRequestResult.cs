﻿using Microsoft.AspNetCore.Mvc;

namespace Coda.API.Result
{
    public class BadRequestResult : IActionResult
    {
        private readonly Error _error;

        public BadRequestResult(string code, string? message)
        {
            _error = new Error()
            {
                Code = code,
                Message = message
            };
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            BadRequest errorContent = new();
            errorContent.Error = _error;

            var objectResult = new ObjectResult(errorContent)
            {
                StatusCode = StatusCodes.Status400BadRequest
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }
    
    public class Error
    {
        public string Code { get; set; }
        public string? Message { get; set; }
    }

    public class BadRequest
    {
        public Error Error { get; set; }
    }
}
